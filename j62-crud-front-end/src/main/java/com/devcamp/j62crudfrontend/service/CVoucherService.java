package com.devcamp.j62crudfrontend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import com.devcamp.j62crudfrontend.model.CVoucher;
import com.devcamp.j62crudfrontend.repository.IVoucherRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class CVoucherService {
    @Autowired
	IVoucherRepository pVoucherRepository;

    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }
    
    public CVoucher getCVoucherById(long id) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
        if (voucherData.isPresent()) {
			return voucherData.get();
		} else {
			return null;
		}
    }

    public CVoucher createCVoucher(CVoucher pVoucher) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(pVoucher.getId());
        if(voucherData.isPresent()) {
            return null;
        } else {
            pVoucher.setNgayTao(new Date());
            pVoucher.setNgayCapNhat(null);            
            return pVoucher;
        }
    }

    public CVoucher updateCVoucherById(long id, CVoucher pVoucher) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			CVoucher voucher = voucherData.get();
			voucher.setMaVoucher(pVoucher.getMaVoucher());
			voucher.setPhanTramGiamGia(pVoucher.getPhanTramGiamGia());
            voucher.setGhiChu(pVoucher.getGhiChu());
			voucher.setNgayCapNhat(new Date());
            return voucher;
		} else {
            return null;
        }
    }

}
